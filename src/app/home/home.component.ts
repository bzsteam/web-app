/**
 * Created by deepak on 09/04/17.
 */
import { Component } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './views/home.component.html',
  styleUrls: ['./styles/home.component.sass']
})
export class HomeComponent {
}
