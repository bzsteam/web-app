/**
 * Created by deepak on 09/04/17.
 */

import {Component, OnInit} from '@angular/core';
import {User, UserService} from "./user.service";
import {Logger} from "../services/logger.service";
import {Router} from "@angular/router";
import {GlobalsService} from "../services/globals.service";
import {Toast} from "../services/toast.service";

@Component({
  moduleId: module.id,
  selector: 'header-custom',
  templateUrl: 'views/header.component.html',
  styleUrls: ['styles/header.component.sass']
})
export class HeaderComponent implements OnInit {
  private user: User = new User();

  constructor(private _userService: UserService, private _logger: Logger, private router: Router, private _globals: GlobalsService) {
    this._userService._user.subscribe(value => this.user = value);
  }


  ngOnInit(): void {

    this._userService.isLoggedIn().subscribe(
      data => {
        this._logger.info("isLoggedIn DATA:", data);
        let tmp = data.data;
        this._userService.setUser(tmp);
      },
      error => {
        this._logger.info("isLoggedIn ERROR:", error);
        Toast.make("Please login to continue");
        this.router.navigate(['/login']);
      }
    );
  }

}
