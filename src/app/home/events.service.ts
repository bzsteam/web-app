/**
 * Created by aditya on 08/03/17.
 */

import {Injectable} from '@angular/core';
import {Http, Response} from "@angular/http";
import {Params} from "../services/params.service";
import {GlobalsService} from "../services/globals.service";
import {Observable, BehaviorSubject} from "rxjs";


export class EventAgg {


  constructor(public _id: string = "",
              public apiKey: string = "",
              public live: {
                countLiveUser: any
              } = {countLiveUser: {}},
              public historic: {
                usersOverTime: any,
                pageVisitCount: any,
                newVsReturningUsers: any,
                avgPageLoadTime: any,
                referrals: any,
                eventCount: any,
                osSpectrum: any,
                languageSpectrum: any,
                mobileSpectrum: any,
                geoSpectrum: any
              } = {
                usersOverTime: {},
                pageVisitCount: {},
                newVsReturningUsers: {},
                avgPageLoadTime: {},
                referrals: {},
                eventCount: {},
                osSpectrum: {},
                languageSpectrum: {},
                mobileSpectrum: {},
                geoSpectrum: {},
              },
              public dateCreated: Date = new Date(),) {
  }

}


@Injectable()
export class EventService {

  private apiURL;  // URL to web api
  constructor(private http: Http, private _params: Params, private _globals: GlobalsService) {
    this.apiURL = _globals.apiURL;
  }


  getAggregateData() {
    let apiURL = this.apiURL + '/events';

    return this.http.get(apiURL)
      .map((res: Response) => {
        let response = res.json();
        response.status = res.status;
        return response;
      })
      .catch((err: Response) => {
        let error = err.json();
        error.status = err.status;
        return Observable.throw(error);
      });
  }


}
