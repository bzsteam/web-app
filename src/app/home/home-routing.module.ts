/**
 * Created by deepak on 09/04/17.
 */

import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {HomeComponent} from './home.component';
import {DashboardComponent} from './dashboard.component';
import {WebApiComponent} from './web-api.component';
import {AndroidApiComponent} from './android-api.component';
import {IosApiComponent} from './ios-api.component';

const appRoutes: Routes = [
  {
    path: 'home', component: HomeComponent,
    children: [
      {path: '', redirectTo: 'dashboard', pathMatch: 'full'},
      {path: 'dashboard', component: DashboardComponent},
      {path: 'web', component: WebApiComponent},
      {path: 'android', component: AndroidApiComponent},
      {path: 'ios', component: IosApiComponent}
    ]
  },
  /*{path: '**', redirectTo: ''}*/
];


@NgModule({
  imports: [
    RouterModule.forChild(appRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class HomeRoutingModule {}
