/**
 * Created by aditya on 08/03/17.
 */

import {Injectable} from '@angular/core';
import {Http, Response} from "@angular/http";
import {Params} from "../services/params.service";
import {GlobalsService} from "../services/globals.service";
import {Observable, BehaviorSubject} from "rxjs";


export class User {



  constructor(public _id: string = "",
              public name: string = "",
              public email: string = "",
              public active: boolean = false,
              public privilege: number = 0,
              public dateCreated: Date = new Date(),
              ) {
  }

}


@Injectable()
export class UserService {

  //private _user: User = new User();
  public _user:BehaviorSubject<User> = new BehaviorSubject<User>(new User());


  private apiURL;  // URL to web api
  constructor(private http: Http, private _params: Params, private _globals: GlobalsService) {
    this.apiURL = _globals.apiURL;
  }


  isLoggedIn() {
    let apiURL = this.apiURL + '/users/isLoggedIn';

    return this.http.get(apiURL)
      .map((res: Response) => {
        let response = res.json();
        response.status = res.status;
        return response;
      })
      .catch((err: Response) => {
        let error = err.json();
        error.status = err.status;
        return Observable.throw(error);
      });
  }

  logout() {
    let apiURL = this.apiURL + '/users/logout';

    return this.http.get(apiURL)
      .map((res: Response) => {
        let response = res.json();
        response.status = res.status;
        return response;
      })
      .catch((err: Response) => {
        let error = err.json();
        error.status = err.status;
        return Observable.throw(error);
      });
  }



  setUser(value: User) {
    this._user.next(value);
    console.log("User set", this._user);
  }

}
