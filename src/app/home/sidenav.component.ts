/**
 * Created by deepak on 09/04/17.
 */

import {Component, OnInit} from '@angular/core';
import {User, UserService} from "./user.service";
import {GlobalsService} from "../services/globals.service";
import {Router} from "@angular/router";
import {Logger} from "../services/logger.service";
import {Toast} from "../services/toast.service";

declare let $: any;

@Component({
  moduleId: module.id,
  selector: 'side-nav',
  templateUrl: 'views/sidenav.component.html',
  styleUrls: ['styles/sidenav.component.sass']
})
export class SideNavComponent implements OnInit {

  private user: User = new User();

  constructor(private _userService: UserService, private _logger: Logger, private router: Router, private _globals: GlobalsService) {
    this._userService._user.subscribe(value => this.user = value);
  }

  ngOnInit(): void {
  }

  logoutUser() {
    this._userService.logout().subscribe(
      data => {
        Toast.make(`Goodbye! ${this.user.name}`);
        this.router.navigate(['/']);
      },
      error => {
        this._logger.info("error", error);
        if (error.message)
          Toast.make(error.message);
        else
          Toast.makeError();
      }
    );
  }

}

