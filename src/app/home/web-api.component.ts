/**
 * Created by deepak on 09/04/17.
 */

import {Component, OnInit} from '@angular/core';
import {User, UserService} from "./user.service";
import {Toast} from "../services/toast.service";
import {GlobalsService} from "../services/globals.service";

declare let $: any;
declare let Clipboard: any;

@Component({
  moduleId: module.id,
  selector: 'web-api',
  templateUrl: 'views/web-api.component.html',
  styleUrls: ['styles/home.component.sass']
})
export class WebApiComponent implements OnInit {
  private user: User = new User();
  private apiURL: string;

  constructor(private _userService: UserService, private _globals: GlobalsService) {
    this._userService._user.subscribe(value => this.user = value);
    this.apiURL = _globals.serverURL + ':8000/api/v1/injector.js';
  }

  ngOnInit(): void {
    let keyClipboard = new Clipboard('#api_key');
    keyClipboard.on('success', function (e) {
      /*console.info('Action:', e.action);
       console.info('Text:', e.text);
       console.info('Trigger:', e.trigger);*/
      Toast.make('API KEY copied to clipboard.');
      e.clearSelection();
    });

    let codeClipboard = new Clipboard('#copy_code_btn');
    codeClipboard.on('success', function (e) {
      Toast.make('Code copied to clipboard.');
      e.clearSelection();
    });
  }

}
