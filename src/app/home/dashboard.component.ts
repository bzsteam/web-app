/**
 * Created by deepak on 09/04/17.
 */

import {Component, NgZone, OnInit} from '@angular/core';
import {EventAgg, EventService} from "./events.service";
import {Logger} from "../services/logger.service";
import {GlobalsService} from "../services/globals.service";
import {Router} from "@angular/router";
import {Toast} from "../services/toast.service";
import {GoogleChartComponent} from "./googleChart.component";

declare let $: any;
declare let google: any;
declare let _: any;

@Component({
  moduleId: module.id,
  selector: 'app-dashboard',
  templateUrl: 'views/dashboard.component.html',
  styleUrls: ['styles/home.component.sass']
})
export class DashboardComponent extends GoogleChartComponent implements OnInit {

  public _event: EventAgg = new EventAgg();
  public google: any = google;
  public lastUpdated: Date = new Date();
  public lastUpdatedDiff: Number = 0;
  public _: any = _;
  public loading: boolean;


  constructor(private _eventService: EventService, private _logger: Logger, private router: Router, private _globals: GlobalsService, private ngZone:NgZone) {
    super();
    this.loading = true;
  }

  ngOnInit(): void {
    //super.ngOnInit();
    setInterval(() => {
      this.lastUpdatedDiff = this.getDiffTime(this.lastUpdated);
      console.log('interval', this.lastUpdatedDiff, this.lastUpdated);
    }, 1000 * 60);

    this.updateData();
    setInterval(() => {
      this.updateData();
    }, 1000 * 30);


    window.onresize = (e) =>
    {
      this.ngZone.run(() => {
        console.log('resizing');
        super.ngOnInit();
      });
    };
  }

  updateData() {
    console.log('updating data');
    Toast.make("Updating data...");
    this._eventService.getAggregateData().subscribe(
      data => {
        this._logger.info("agg DATA:", data);
        this._event = data.data;
        this.lastUpdated = new Date();
        super.ngOnInit(); //to initialize graphs
      },
      error => {
        this._logger.info("agg ERROR:", error);
      }
    );
  }

  getDiffTime(date: any) {
    let today: any = new Date();
    let diffMs = today - date; // milliseconds between now & Christmas
    let diffMins = Math.round(((diffMs % 86400000) % 3600000) / 60000); // minutes
    return diffMins;
  }

  drawGraph() {
    this.drawLiveUsersChart();
    this.drawUsersOverTimeChart();
    this.drawReferralsChart();
    this.drawPageVisitCount();
    this.drawOsSpectrumChart();
    this.drawUserReturnChart();
    this.drawMobileSpectrumChart();
    this.drawLanguageSpectrumChart();
    this.drawEventCountChart();
    this.drawEventCountChart2();
    this.drawAvgPageLoadBarChart();
    this.drawGeoChart();
    this.loading = false;
  }

  drawLiveUsersChart() {

    let values = [];
    values.push(['Label', 'Value']);

    //this._event.live.countLiveUser = 915;
    values.push(['Live Users', this._event.live.countLiveUser]);

    console.log('values', values);
    let data = super.createDataTable(values);

    let rand = Math.random();
    let x = this._event.live.countLiveUser;
    let max = x > 100 ? Math.ceil(x + x * rand) : 100;
    let options = {
      title: 'Live Users',
      max: max,
      greenFrom: 0, greenTo: max * .75,
      yellowFrom: max * .75, yellowTo: max * .90,
      redFrom: max * .90, redTo: max
    };

    let chart = super.createGaugeChart(document.getElementById('live_users_gauge_chart_div'));
    chart.draw(data, options);
  }

  drawUsersOverTimeChart() {

    let values = [];
    let valuesTmp = [];
    values.push(['Date', 'Users']);

    for (let tmp of this._event.historic.usersOverTime) {
      valuesTmp.push([new Date(tmp._id.date), tmp.count]);
    }

    valuesTmp = _(valuesTmp).sortBy(function (a) {
      return a[0];
    });

    console.log('sortedValues', valuesTmp);

    for (let tmp of valuesTmp) {
      values.push(tmp);
    }


    console.log('values', values);
    let data = super.createDataTable(values);

    let options = {
      title: 'Users Over Time',
      legend: {position: 'top'},
      hAxis: {
        title: 'Date'
      },
      vAxis: {
        title: 'Users'
      },
      crosshair: {
        color: '#000',
        trigger: 'selection'
      }
    };

    let chart = super.createLineChart(document.getElementById('user_over_time_line_chart_div'));
    chart.draw(data, options);
  }

  drawReferralsChart() {

    let values = [];
    values.push(['Source', 'Count']);

    for (let key in this._event.historic.referrals) {
      values.push([key, this._event.historic.referrals[key]]);
    }

    console.log('values', values);
    let data = super.createDataTable(values);

    let options = {
      title: 'Sources',
      pieHole: 0.4,
    };

    let chart = super.createPieChart(document.getElementById('referrals_pie_chart_div'));
    chart.draw(data, options);
  }

  drawPageVisitCount() {

    let values = [];
    values.push(['Page Title', 'Count', {role: 'style'}]);

    for (let tmp of this._event.historic.pageVisitCount) {
      values.push([tmp._id.pageTitle, tmp.count, this.getRandomColor()]);
    }

    console.log('values', values);
    let data = super.createDataTable(values);

    let options = {
      chart: {
        title: 'Pages Visit Count'
      },
      bars: 'vertical'
    };

    let chart = super.createBarChart(document.getElementById('page_visit_bar_chart_div'));
    chart.draw(data, options);
  }

  drawOsSpectrumChart() {

    let values = [];
    values.push(['Operating System', 'Count']);

    for (let tmp of this._event.historic.osSpectrum) {
      values.push([tmp._id, tmp.count]);
    }
    console.log('values', values);
    let data = super.createDataTable(values);

    let options = {
      title: 'Operating Systems',
      pieHole: 0.4,
    };

    let chart = super.createPieChart(document.getElementById('os_spectrum_pie_chart_div'));
    chart.draw(data, options);
  }

  drawUserReturnChart() {

    let values = [];
    values.push(['Users', 'Count']);

    values.push(['New', this._event.historic.newVsReturningUsers.new]);
    values.push(['Returning', this._event.historic.newVsReturningUsers.returning]);

    console.log('values', values);
    let data = super.createDataTable(values);

    let options = {
      title: 'Users',
      pieHole: 0.4,
    };

    let chart = super.createPieChart(document.getElementById('new_vs_returning_pie_chart_div'));
    chart.draw(data, options);
  }

  drawMobileSpectrumChart() {

    let values = [];
    values.push(['Platform', 'Count']);

    values.push(['Desktop', this._event.historic.mobileSpectrum.desktop]);
    values.push(['Mobile', this._event.historic.mobileSpectrum.mobile]);

    console.log('values', values);
    let data = super.createDataTable(values);

    let options = {
      title: 'Platforms',
      is3D: true
    };

    let chart = super.createPieChart(document.getElementById('mobile_spectrum_pie_chart_div'));
    chart.draw(data, options);
  }

  drawLanguageSpectrumChart() {

    let values = [];
    values.push(['Languages', 'Count', {role: 'style'}]);

    for (let tmp of this._event.historic.languageSpectrum) {
      values.push([tmp._id, tmp.count, this.getRandomColor()]);
    }

    console.log('values', values);
    let data = super.createDataTable(values);

    let options = {
      title: 'Languages',
      is3D: true
    };

    let chart = super.createPieChart(document.getElementById('language_spectrum_pie_chart_div'));
    chart.draw(data, options);
  }

  drawEventCountChart() {

    let values = [];
    values.push(['Events Count', 'Total Count', {role: 'style'}]);

    for (let tmp of this._event.historic.eventCount) {
      values.push([tmp._id, tmp.count, this.getRandomColor()]);
    }

    console.log('values', values);
    let data = super.createDataTable(values);

    let options = {
      chart: {
        title: 'Events Count'
      },
      bars: 'vertical'
    };

    let chart = super.createBarChart(document.getElementById('event_count_bar_graph_div'));
    chart.draw(data, options);
  }

  drawEventCountChart2() {

    let values = [];
    values.push(['Events Count', 'Total Count', {role: 'style'}]);

    for (let tmp of this._event.historic.eventCount) {
      values.push([tmp._id, tmp.count, this.getRandomColor()]);
    }

    console.log('values', values);
    let data = super.createDataTable(values);

    let options = {
      chart: {
        title: 'Events Count'
      },
      bars: 'vertical'
    };

    let chart = super.createBarChart(document.getElementById('event_count_bar_graph_div2'));
    chart.draw(data, options);
  }

  drawAvgPageLoadBarChart() {

    let values = [];
    values.push(['Page Title', 'Average Load Time(ms)', {role: 'style'}]);

    for (let tmp of this._event.historic.avgPageLoadTime) {
      values.push([tmp._id.pageTitle, tmp.avgLoadTime, this.getRandomColor()]);
    }

    console.log('values', values);
    let data = super.createDataTable(values);

    let options = {
      chart: {
        title: 'Pages Average Load Time',
        subtitle: '(in ms)'
      },
      bars: 'vertical'
    };

    let chart = super.createBarChart(document.getElementById('avg_load_time_bar_graph_div'));
    chart.draw(data, options);
  }

  drawGeoChart() {

    let values = [];
    values.push(['Country', 'Popularity']);

    for (let tmp of this._event.historic.geoSpectrum) {
      values.push([tmp._id.countryName, tmp.count]);
    }

    console.log('values', values);
    let data = super.createDataTable(values);

    let options = {};

    let chart = super.createGeoChart(document.getElementById('geo_graph_div'));
    chart.draw(data, options);
  }

  getRandomColor() {
    let letters = '0123456789ABCDEF';
    let color = '#';
    for (let i = 0; i < 6; i++) {
      color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
  }

}

