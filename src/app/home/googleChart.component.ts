/**
 * Created by deepak on 09/04/17.
 */

import { Component, OnInit} from '@angular/core';
declare let google:any;
@Component({
  selector: 'chart'
})
export class GoogleChartComponent implements OnInit {
  private static googleLoaded:any;

  constructor(){
    console.log("Here is GoogleChartComponent")
  }

  getGoogle(): any {
    return google;
  }
  ngOnInit() {
    console.log('ngOnInit');
    if(!GoogleChartComponent.googleLoaded) {
      GoogleChartComponent.googleLoaded = true;
      google.charts.load('current',  {packages: ['corechart', 'bar', 'geochart', 'gauge']});
    }
    google.charts.setOnLoadCallback(() => this.drawGraph());
  }

  drawGraph(){
    console.log("DrawGraph base class!!!! ");
  }
  createGaugeChart(element:any):any {
    //return new google.visualization.BarChart(element);
    return new google.visualization.Gauge(element);
  }
  createLineChart(element:any):any {
    //return new google.visualization.BarChart(element);
    return new google.visualization.LineChart(element);
  }
  createBarChart(element:any):any {
    //return new google.visualization.BarChart(element);
    return new google.charts.Bar(element);
  }
  createGeoChart(element:any):any {
    return new google.visualization.GeoChart(element);
  }
  createPieChart(element:any):any {
    return new google.visualization.PieChart(element);
  }

  createDataTable(array:any[]):any {
    return google.visualization.arrayToDataTable(array);
  }
}
