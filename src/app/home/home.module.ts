/**
 * Created by deepak on 09/04/17.
 */
import {FormsModule} from '@angular/forms';
import {CommonModule} from '@angular/common';
import {HomeComponent} from './home.component';
import {NgModule} from '@angular/core';
import {HomeRoutingModule} from './home-routing.module';
import {DashboardComponent} from './dashboard.component';
import {SideNavComponent} from './sidenav.component';
import {HeaderComponent} from './header.component';
import {WebApiComponent} from './web-api.component';
import {AndroidApiComponent} from './android-api.component';
import {IosApiComponent} from './ios-api.component';
import {GroupByPipe} from "../pipes/groupBy.pipe";
import {EqualsPipe} from "../pipes/equals.pipe";
import {SearchPipe} from "../pipes/search.pipe";
import {OrderByPipe} from "../pipes/orderBy.pipe";
import {UserService} from "./user.service";
import {EventService} from "./events.service";
import {MarkdownModule} from "angular2-markdown";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    MarkdownModule.forRoot(),
    HomeRoutingModule
  ],
  exports: [],
  declarations: [
    HomeComponent,
    DashboardComponent,
    WebApiComponent,
    AndroidApiComponent,
    IosApiComponent,
    SideNavComponent,
    HeaderComponent,
    OrderByPipe,
    SearchPipe,
    EqualsPipe,
    GroupByPipe,
  ],
  providers: [UserService, EventService],
})
export class HomeModule {
}
