/**
 * Created by deepak on 09/04/17.
 */
import { Component } from '@angular/core';

@Component({
  selector: 'app-index',
  templateUrl: './views/index.component.html',
  styleUrls: ['./styles/index.component.sass']
})
export class IndexComponent {
}
