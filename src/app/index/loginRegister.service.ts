/**
 * Created by aditya on 07/03/17.
 */

import {Injectable} from '@angular/core';
import {Http, Response, URLSearchParams, RequestOptions}       from '@angular/http';
import 'rxjs/add/operator/map';
import {Params} from "../services/params.service";
import {GlobalsService} from "../services/globals.service";
import {Observable} from "rxjs";


@Injectable()
export class LoginRegisterService {

  private apiURL;  // URL to web api
  constructor(private http: Http, private _params: Params, private _globals: GlobalsService) {
    this.apiURL = _globals.apiURL + '/users';
  }

  register(user: any): Observable<any> {
    return this.http.post(this.apiURL, user)
      .map((res: Response) => {
        let response = res.json();
        response.status = res.status;
        return response;
      })
      .catch((err: Response) => {
        let error = err.json();
        error.status = err.status;
        //console.log("ERROR:",details);
        //return Observable.throw(new Error(details));
        return Observable.throw(error);
      });
  }

  login(user: any): Observable<any> {
    let params: URLSearchParams = this._params.objectToURLSearchParams(user);

    return this.http.get(this.apiURL, {search: params})
      .map((res: Response) => {
        let response = res.json();
        response.status = res.status;
        return response;
      })
      .catch((err: Response) => {
        let error = err.json();
        error.status = err.status;
        //console.log("ERROR:",details);
        //return Observable.throw(new Error(details));
        return Observable.throw(error);
      });
  }
}
