/**
 * Created by deepak on 09/04/17.
 */

import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {IndexComponent} from './index.component';
import {LandingComponent} from './landing.component';
import {LoginComponent} from './login.component';
import {RegisterComponent} from './register.component';

const appRoutes: Routes = [
  {
    path: '', component: IndexComponent,
    children: [
      {path: '', component: LandingComponent},
      {path: 'login', component: LoginComponent},
      {path: 'register', component: RegisterComponent}
    ]
  },
  /*{path: '**', redirectTo: ''}*/
];


@NgModule({
  imports: [
    RouterModule.forChild(appRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class IndexRoutingModule {}
