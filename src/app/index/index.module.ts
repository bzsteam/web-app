/**
 * Created by deepak on 09/04/17.
 */
import {FormsModule} from '@angular/forms';
import {CommonModule} from '@angular/common';
import {IndexComponent} from './index.component';
import {NgModule} from '@angular/core';
import {IndexRoutingModule} from './index-routing.module';
import {LandingComponent} from './landing.component';
import {LoginComponent} from './login.component';
import {RegisterComponent} from './register.component';
import {HttpModule} from "@angular/http";
import {LoginRegisterService} from "./loginRegister.service";
import {Toast} from "../services/toast.service";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    HttpModule,
    IndexRoutingModule
  ],
  exports: [],
  declarations: [
    IndexComponent,
    LandingComponent,
    LoginComponent,
    RegisterComponent
  ],
  providers: [LoginRegisterService],
})
export class IndexModule {
}
