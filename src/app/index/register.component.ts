/**
 * Created by deepak on 09/04/17.
 */

import {Component, OnInit} from '@angular/core';
import {Observable} from "rxjs";
import {Logger} from "../services/logger.service";
import {LoginRegisterService} from "./loginRegister.service";
import {Router} from "@angular/router";
import {Toast} from "../services/toast.service";

declare let $: any;

@Component({
  moduleId: module.id,
  selector: 'app-register',
  templateUrl: 'views/register.component.html',
  styleUrls: ['styles/register.component.sass']
})
export class RegisterComponent implements OnInit {
  user = {
    name: '',
    email: '',
    password: '',
    confirmPassword: ''
  };

  constructor(private _logger: Logger, private _logReg: LoginRegisterService, private router: Router) {
  }

  ngOnInit(): void {
  }

  clearUser(): void {
    this.user = {
      name: '',
      email: '',
      password: '',
      confirmPassword: ''
    };
  }

  register() {
    this._logger.log("register", this.user);
    this._logReg.register(this.user).subscribe(
      data => {
        // refresh the list
        this._logger.info("data", data);
        //toast("Registration complete. Please check your inbox for further instructions.", 5000, 'rounded');
        Toast.make("Registration complete. Please continue to login.");
        this.clearUser();
        return true;
      },
      error => {
        this._logger.info("error", error);
        //toast(error.message, 3000, 'rounded');
        if (error.message)
          Toast.make(error.message);
        else
          Toast.makeError();

        return Observable.throw(error);
      }
    );
  }


}


