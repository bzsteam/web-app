/**
 * Created by deepak on 10/03/17.
 */

import {Component, OnInit} from '@angular/core';

declare var $: any;

@Component({
  moduleId: module.id,
  selector: 'app-landing',
  templateUrl: 'views/landing.component.html',
  styleUrls: ['styles/index.component.sass']
})
export class LandingComponent implements OnInit {
  constructor() {
  }

  ngOnInit(): void {
  }

}

