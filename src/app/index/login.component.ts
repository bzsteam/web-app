/**
 * Created by deepak on 09/04/17.
 */

import {Component, OnInit} from '@angular/core';
import {Logger} from "../services/logger.service";
import {LoginRegisterService} from "./loginRegister.service";
import {Router} from "@angular/router";
import {Observable} from "rxjs";
import {Toast} from "../services/toast.service";

declare let $: any;

@Component({
  moduleId: module.id,
  selector: 'app-login',
  templateUrl: 'views/login.component.html',
  styleUrls: ['styles/register.component.sass']
})
export class LoginComponent implements OnInit {

  user = {
    name: '',
    email: '',
    password: '',
    confirmPassword: ''
  };

  constructor(private _logger: Logger, private _logReg: LoginRegisterService, private router: Router) {
  }

  ngOnInit(): void {
  }

  clearUser(): void {
    this.user = {
      name: '',
      email: '',
      password: '',
      confirmPassword: ''
    };
  }

  login() {
    this._logger.log("login", this.user);
    this._logReg.login(this.user).subscribe(
      data => {
        // refresh the list
        this._logger.info("data", data);
        $('#loginModal').modal('close');
        Toast.make("Welcome " + this.user.email);
        this.clearUser();
        this.router.navigate(['/home']);
      },
      error => {
        this._logger.info("error", error);
        if (error.message)
          Toast.make(error.message);
        else
          Toast.makeError();
      }
    );
  }
}


