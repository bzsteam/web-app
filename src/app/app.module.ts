import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import {HttpModule, RequestOptions} from '@angular/http';

import { AppComponent } from './app.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {AppRoutingModule} from './app-routing.module';
import {HomeModule} from './home/home.module';
import {IndexModule} from './index/index.module';
import {Logger} from "./services/logger.service";
import {Params} from "./services/params.service";
import {GlobalsService} from "./services/globals.service";
import {CustExtBrowserXhr, CustomBaseRequestOptions} from "./services/customBaseRequestOptions.service";
import {Toast} from "./services/toast.service";



@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    NgbModule.forRoot(),
    IndexModule,
    HomeModule,
    AppRoutingModule
  ],
  providers: [Logger, Toast, Params, GlobalsService, { provide: RequestOptions, useClass: CustomBaseRequestOptions}],
  bootstrap: [AppComponent]
})
export class AppModule { }
