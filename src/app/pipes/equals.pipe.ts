/**
 * Created by aditya on 09/03/17.
 */

import {Pipe, PipeTransform, Injectable} from "@angular/core";


let deep_value = function (obj, paths) {
  for (let i = 0, path = paths.split('.'), len = path.length; i < len; i++) {
    obj = obj[path[i]];
  }
  return obj;
};


@Pipe({
  name: 'equalFilter',
  pure: false
})
@Injectable()
export class EqualsPipe implements PipeTransform {

  transform(items: any, searchOn: any, term: any, omit: any): any {
    if (term === undefined) return items;
    if(term === omit) return items;

    return items.filter(function (item) {
      //console.log("search", deep_value(item ,searchOn));
      return deep_value(item, searchOn).toString().toLowerCase() === term.toString().toLowerCase();
    })
  }
}
