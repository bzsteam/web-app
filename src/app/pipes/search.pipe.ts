/**
 * Created by aditya on 09/03/17.
 */

import {Pipe, PipeTransform, Injectable} from "@angular/core";


let deep_value = function (obj, paths) {
  for (let i = 0, path = paths.split('.'), len = path.length; i < len; i++) {
    obj = obj[path[i]];
  }
  return obj;
};


@Pipe({
  name: 'searchFilter',
  pure: false
})
@Injectable()
export class SearchPipe implements PipeTransform {

  transform(items: any, searchOn: any, term: any): any {
    if (term === undefined || !term) return items;
    if (searchOn === undefined || !searchOn) return items;

    searchOn = searchOn.trim();
    term = term.toLowerCase().trim();
    console.log("test1", searchOn, term);


    return items.filter(function (item) {
      console.log("test2", deep_value(item ,searchOn));
      if (deep_value(item, searchOn))
        return deep_value(item, searchOn).toString().toLowerCase().trim().includes(term);
      else return false;
    })
  }
}
