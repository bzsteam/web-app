/**
 * Created by aditya on 06/03/17.
 */

import {Injectable} from '@angular/core';
import {isPrimitive} from "@angular/core/src/facade/lang";
import {URLSearchParams} from "@angular/http";

/**
 * Converts an object to a parametrised string.
 * @param object
 * @returns {string}
 */
@Injectable()
export class Params {

  public objectToParams(object): string {
    return Object.keys(object).map((value) => {
      let objectValue = isPrimitive(object[value]) ? object[value] : JSON.stringify(object[value]);
      return `${value}=${objectValue}`;
    }).join('&');
  }

  public objectToURLSearchParams(object): URLSearchParams {

    let params: URLSearchParams = new URLSearchParams();

    Object.keys(object).map((key) => {
      let value = isPrimitive(object[key]) ? object[key] : JSON.stringify(object[key]);
      params.set(key, value);
      //return `${value}=${objectValue}`;

    });

    return params;
  }

}
