/**
 * Created by aditya on 08/03/17.
 */

import {Injectable} from '@angular/core';

declare let $: any;
declare let window: any;

@Injectable()
export class GlobalsService {


  readonly serverURL: string;
  readonly apiURL: string;
  //navBarMobile = false;


  constructor() {
      this.serverURL = `http://${window.location.hostname}`;
      this.apiURL = `http://${window.location.hostname}:3000`;
  }

  /*toggleNavBarMobile(state: boolean = true) {
    //console.log("toggle nav bar", state);
    this.navBarMobile = !this.navBarMobile;
  }

  closeNavBar(e: Event) {
    //console.log("close nav bar", e['toElement'].id, $('#toggle_nav_button')[0].id);
    if (e['toElement'].id !== $('#toggle_nav_button')[0].id)
      this.navBarMobile = false;
  }*/

}
