/**
 * Created by aditya on 09/04/17.
 */

import {Injectable} from '@angular/core';

declare let $: any;


@Injectable()
export class Toast {
  static make(msg: string) {
    return $.toast({
      text: msg,
      position : 'top-right',
      showHideTransition : 'fade',  // It can be plain, fade or slide
      hideAfter : 5000,
    });
  }

  static makeError(msg: string = "Sorry, An error occurred!") {
    return $.toast({
      text: msg,
      position : 'top-right',
      showHideTransition : 'fade',  // It can be plain, fade or slide
      hideAfter : 5000,
    });
  }

  static makeCustom(config: any) {
    return $.toast(config);
  }
}
