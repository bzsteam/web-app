/**
 * Created by aditya on 06/03/17.
 */

import { Injectable } from '@angular/core';

@Injectable()
export class Logger{
  log(msg: any, ...others ){ console.log(msg, ...others); }
  error(msg: any, ...others ){ console.error(msg, ...others); }
  warn(msg: any, ...others ){ console.warn(msg, ...others); }
  info(msg: any, ...others ){ console.info(msg, ...others); }
}
