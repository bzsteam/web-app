/**
 * Created by deepak on 09/04/17.
 */

import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';

const appRoutes: Routes = [
  /*{
   path: 'okay',
   redirectTo: '',
   pathMatch: 'full'
   }*/
  {path: '**', redirectTo: ''}
];


@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule {}
