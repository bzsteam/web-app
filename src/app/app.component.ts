import { Component } from '@angular/core';
import {Logger} from "./services/logger.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass'],
  providers: [Logger]
})
export class AppComponent {
  constructor(private logger: Logger) {
    logger.info("App constructed");
  }
}
