# Create image based on the official Node 6 image from dockerhub
FROM node:6

# Install Dumb-Init
ADD https://github.com/Yelp/dumb-init/releases/download/v1.1.1/dumb-init_1.1.1_amd64 /usr/local/bin/dumb-init
RUN chmod +x /usr/local/bin/dumb-init

# Create a directory where our app will be placed
RUN mkdir -p /usr/src/app

# Change directory so that our commands run inside this new directory
WORKDIR /usr/src/app

# Copy dependency definitions
COPY package.json /usr/src/app

# Install dependecies
RUN npm install

# Get all the code needed to run the app
COPY . /usr/src/app

# Expose the port the app runs in
EXPOSE 4000

#Create non root user
RUN groupadd -r docker \
   && useradd -m -r -g docker docker

RUN chmod a+rwx /usr/src/app
#RUN chown -R docker:docker /usr/src/app

USER docker

# Serve the app
CMD ["dumb-init", "npm", "run", "build:serve"]
